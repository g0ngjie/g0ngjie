---
title: "设置某个exe文件为启动项"
date: 2022-06-04T14:44:06+08:00
draft: false
categories: ["window"] # 前端 linux
tags: ["system"] # js seo docker arch git vue
---

## Window10

打开这个文件夹

C:\ProgramData\Microsoft\Windows\Start Menu\Programs\StartUp

然后把需要开机自启的 exe 快捷方式拷贝进去即可

如果上面文件夹打不开，可以 win+r 输入

shell:startup 就可以打开启动文件夹了
