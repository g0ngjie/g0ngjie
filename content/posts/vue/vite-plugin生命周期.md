---
title: "Vite Plugin生命周期"
date: 2022-06-17T10:47:36+08:00
draft: false
categories: ["前端"] # 前端 linux window
tags: ["vue"] # js seo docker arch git vue system
---

## Vite Plugin 插件钩子

### 通用钩子

开发时，`Vite dev server` 创建一个插件容器按照`Rollup`调用创建钩子的规则请求各个钩子函数。

下面钩子会在服务器启动时调用一次

- `options` 替换或槽中 `rollup` 选项
- `buildStart` 开始创建

下面钩子每次有模块请求时都会呗调用：

- `resolveId`创建自定义确认函数，常用语定位第三方依赖
- `load`创建自定义加载函数，可用于返回自定义内容
- `transform`可用于转换已加载的模块内容

下面钩子会在服务器关闭时调用一次

- `buildEnd`
- `closeBundle`

### Vite 特有钩子

- `config`：修改 Vite 配置
- `configResolved`：Vite 配置确认
- `configureServer`：用于配置 dev server
- `transformIndexHtml`：用于转换宿主页
- `handleHotUpdate`：自定义 HMR 更新时间

### 范例：钩子调用顺序测试

```js
export default function myExample() {
  // 返回的时插件对象
  return {
    name: "my-example",
    // 初始化hooks，只走一次
    options(opts) {
      console.log("options", opts);
    },
    buildStart() {
      console.log("buildStart");
    },
    // vite特有钩子
    config(config) {
      console.log("config", config);
      // 返回对象会合并在默认config中
      return {};
    },
    configResolved(resolvedConfig) {
      console.log("configResolved");
    },
    configureServer(server) {
      console.log("configureServer");
      // server.app.use((req, res, next) => {
      //     // custom middleware
      //     // console.log('configureServer req', req)
      //     // next()
      // })
    },
    transformIndexHTML(html) {
      console.log("transformIndexHTML");
      return html;
      // return html.replace(
      //     /<title>.*?<\/title>/,
      //     '<title>Title replaced!</title>'
      // )
    },
    // id确认
    resolveId(source) {
      console.log("source");
      if (source === "virtual-module") {
        return source; // 返回source 表明命中，vite不在询问其他插件处理该id请求
      }
      return null; // 返回null 表明是其他id要继续处理
    },
    // 加载模块代码
    load(id) {
      if (id === "virtual-module") {
        return 'export default "This is virtual!"';
      }
      return null;
    },
    // 转换
    transform(code, id) {
      if (id === "virtual-module") {
        console.log("transform", code);
      }
      return code;
    },
  };
}
```

### 钩子调用顺序

`config` -> `configResolved` -> `options` -> `configureServer` -> `buildStart` -> `Vite dev server ready` -> `transformIndexHtml` -> `resolvedId` -> `load` -> `transform`

### 插件顺序

- 别名处理 Alias
- 用户插件设置 `enforce: 'pre'`
- Vite 核心插件
- 用户插件未设置 `enforce`
- Vite 构建插件
- 用户插件设置 `enforce: 'post'`
- Vite 构建后置插件(minify, manifest, reporting)
