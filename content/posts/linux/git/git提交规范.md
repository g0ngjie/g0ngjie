---
title: "Git提交规范"
date: 2022-06-04T14:42:23+08:00
draft: false
categories: ["linux"] # 前端 linux
tags: ["git"] # js seo docker arch git vue
---

## 用于说明 commit 的类别

```
feat：新功能（feature）

fix：修补 bug

docs：文档（documentation）

style： 格式（不影响代码运行的变动）

refactor：重构（即不是新增功能，也不是修改 bug 的代码变动）

test：增加测试

chore：构建过程或辅助工具的变动
```
