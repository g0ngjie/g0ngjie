---
title: "Docker常用命令"
date: 2022-02-24T13:13:15+08:00
categories: ["linux"]
tags: ["docker"]
---

### 列出悬空镜像

```shell
docker images -f dangling=true
```

### 清理悬空镜像

```shell
docker rmi $(docker images -f "dangling=true" -q)
```
