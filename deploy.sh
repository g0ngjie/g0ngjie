#!/bin/bash

rm -rf public
git submodule update --remote --merge
hugo
git add .
git commit -m "deploy"
git push origin master
# 文档地址 https://hugodoit.pages.dev/zh-cn/categories/documentation/
