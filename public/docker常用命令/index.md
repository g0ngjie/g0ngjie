# Docker常用命令


### 列出悬空镜像

```shell
docker images -f dangling=true
```

### 清理悬空镜像

```shell
docker rmi $(docker images -f "dangling=true" -q)
```

